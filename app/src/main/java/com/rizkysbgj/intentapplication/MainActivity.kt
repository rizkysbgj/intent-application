package com.rizkysbgj.intentapplication

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnStartActivity: Button = findViewById(R.id.btnStartActivity)
        btnStartActivity.setOnClickListener(this)

        val btnStartActivityData: Button = findViewById(R.id.btnStartActivityData)
        btnStartActivityData.setOnClickListener(this)

//        val btnStartCamera: Button = findViewById(R.id.btnStartCamera)
//        btnStartCamera.setOnClickListener(this)

        val btnStartDialer: Button = findViewById(R.id.btnStartDialer)
        btnStartDialer.setOnClickListener(this)
     }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnStartActivity -> {
                val intent: Intent = Intent(this, SecondActivity::class.java)
                startActivity(intent)
            }

            R.id.btnStartActivityData -> {
                val intent = Intent(this, DataActivity::class.java)
                intent.putExtra(DataActivity.EXTRA_NAME, "Rizky Subagja")
                intent.putExtra(DataActivity.EXTRA_AGE, 21)
                startActivity(intent)
            }

//            R.id.btnStartCamera -> {
//                val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
//                startActivity(intent)
//            }

            R.id.btnStartDialer -> {
                val intent = Intent(Intent.ACTION_DIAL)
                startActivity(intent)
            }
        }
    }
}
